import psycopg2

conn = psycopg2.connect("dbname=lab host=localhost password=postgres user=postgres")

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"
check_user_inventory = "SELECT SUM(amount) FROM Inventory WHERE playername = %(username)s"
buy_increment_inventory = "UPDATE Inventory SET amount = amount + %(amount)s WHERE playername = %(username)s AND productname = %(product)s"
inventory_request = "SELECT amount FROM Inventory WHERE playername = %(username)s"


def buy_product(username, product, amount):
    with conn:
        with conn.cursor() as cur:
            obj = {"product": product, "username": username, "amount": amount}


            # ADD A COUPLE OF TEST RESOURCES

            cur.execute("UPDATE Shop SET in_stock = 100 WHERE product = 'banana'")
            cur.execute("UPDATE Shop SET in_stock = 80 WHERE product = 'milk'")
            cur.execute("UPDATE Shop SET in_stock = 10 WHERE product = 'marshmello'")
            cur.execute("UPDATE Player SET balance = 500 WHERE username = 'Alice'")
            cur.execute("UPDATE Player SET balance = 200 WHERE username = 'Bob'")
            cur.execute("INSERT INTO Shop (product, in_stock, price) VALUES ('banana', 100, 100);")
            cur.execute("INSERT INTO Shop (product, in_stock, price) VALUES ('milk', 80, 80);")
            cur.execute("INSERT INTO Inventory (playername, productname, amount) VALUES ('Alice', 'banana', '50');")
            cur.execute("INSERT INTO Inventory (playername, productname, amount) VALUES ('Bob', 'milk', '40');")

            try:
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Bad balance")

            try:
                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Product is out of stock")

            try:
                cur.execute(check_user_inventory, obj)
                if (cur.fetchone()[0] + amount) > 100:
                    raise Exception("Inventory exceeded limit")
                else:
                    cur.execute(buy_increment_inventory, obj)
            except psycopg2.errors.CheckViolation as e:
                raise Exception("Wrong product or wrong username")

            
            # CLEAN UP OF THE TEST RESOURCES

            cur.execute("DELETE FROM Inventory WHERE playername = 'Alice' AND productname = 'banana';")
            cur.execute("DELETE FROM Inventory WHERE playername = 'Bob' AND productname = 'milk';")
            cur.execute("DELETE FROM Shop WHERE product = 'banana';")
            cur.execute("DELETE FROM Shop WHERE product = 'milk';")

            conn.commit()

buy_product("Alice", "banana", 2)